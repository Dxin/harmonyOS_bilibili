# 河南职业技术学院授课

## 目录

1. `lessCode` 

   >  低代码学习鸿蒙。快速演示，仅仅为演示作用。[低代码视频指导](https://www.bilibili.com/video/BV1pb4y1g75m?p=9)

2. `HnzjBase`

   > 基础语法教学：内容s顺序见`src/main/ets/common/Contants.ets`

3. `LightGreen`

   > 项目学习：何浅绿诗词项目。http模块+布局等。

> 有任何需要支持的地方，可联系帝心老师一起探讨学习。