---

slug: 欲买桂花同载酒，且将新火试新茶。

title: 欲买桂花同载酒，且将新火试新茶

authors: [mayuanwei]

tags: [匆匆那年]

---

宿舍里新来了一个婚礼司仪。

在屋里里练习婚礼主持场景的稿子。

言辞间抑扬顿挫，仿佛读出了一场跌宕起伏的爱情。

我侧着耳朵听，人声鼎沸的盛大婚礼便呈现在眼前。

---

<!--truncate-->

趁着欢快轻盈的背景音乐，在主持人的开场引导下，新人出场。

亲朋好友齐聚一堂，红包已然给过，奉上些大概百年好合，子孙满堂的祝福。

坐看新人满眼幸福，交换戒指，深情地倒满香槟塔。

酒已敬过，美味佳肴摆满桌子，大家继续喝酒吃菜。

  <br/>

好一场盛宴宏大，好一桩喜事美满。

 

  <br/>

美好的事情总能让人意淫出自己就是主角。

于是我开始想，自己的婚礼该是怎样的场景。自己想要的婚礼又是怎样呢？

  <br/>

我想，当故事只有经过岁月的发酵，当自己放过故事里的自己。只有如今的自己能够做到波澜不惊地嘲讽当初的不知所措。我才在今天决定写出那些过往。

 
 <br/>
 

大概几年前的某个下午，阳光明媚。

跟文彤彤坐在小饭店的靠窗位置吃饭。

阳光透过窗子打在她的秀发上，侧脸上，肩头及衬衣。

仿佛发光的天使。特别美好。

一瞬间，我心里砰砰跳，默念着，就这样，就这样，一辈子多好。

  <br/>

我往嘴里扒拉着米饭。然后喋喋不休地向她描述：

据说表白与求婚的方式有很多。

有盛大的蜡烛盛宴。也有茶余饭后的喃喃细语。

如果某天，我希望自己的方式跟别人都不同。

我想要的是那种两个人一起洗菜做菜。然后两三热菜端上桌。

米饭飘着香味，你夹一口菜放进嘴里夸：真好吃。

我扒拉一口米饭，轻轻的说：我们结婚吧。

然后舔掉嘴边的米粒，继续吃。

你又夹一口菜放进嘴里说：好啊，刚好我明天有空。去民政局。

于是我们吃完自己做的菜，我去刷碗，你去翻抽屉，找户口本。

 
 <br/>
文彤彤那天听我喋喋不休地描述这样的场景，抬起头看了我一会。

然后夹一块红烧茄子放进嘴里。她说：真好吃。

我往嘴里扒拉着米饭，吃的嘴角沾着米粒。
 <br/>
 

文成去天津读了天工大，他骗文彤彤说天津九条河，螃蟹比米贱。

文彤彤就跑来告诉我说要去天津吃螃蟹。

12年夏天的塘沽坝热的厉害，风从海上来，吹动她还未留长的秀发。好看极了。

  <br/>

文彤彤说：螃蟹这么贵，那天津的米一定更贵了。

我告诉她：天津九条河，螃蟹比米贱，说的是上世纪三四十年代天津人民的生活状态。

当时的文彤彤觉得我好厉害。知道的真多。

  <br/>

从天津离开的时候，文成带我们吃了小宝栗子。

文彤彤说自己超级喜欢吃栗子。

 

 
 <br/>
后来去往北方，千里之外的齐齐哈尔冷的早。十月份刚过，气温就降下来。

文彤彤打电话告诉我要记得买厚衣服，不要冻坏了。

我说：我饭都吃不饱。哪有钱买衣服。

于是文彤彤给我买了过冬的衣服和鞋子寄过来。

  <br/>

文彤彤告诉我要记得给她打电话。

我说：我饭都吃不饱，哪有钱打电话。

于是每个月文彤彤都给我充话费。

  <br/>

我有了新衣服也有了电话费。我好开心。

文彤彤告诉我，我给你分了一半生活费，饭要吃饱。

我开心地跳脚。

  <br/>

有一天我突然发现自己的衣服鞋子都是文彤彤送的。我的话费都是文彤彤冲的。

我告诉文彤彤，我给你买衣服吧。

文彤彤说：你饭都吃不饱，我不要你给我买东西。

我想了想觉得有道理。我还是把钱留起来吃饭好了。

  <br/>

16年九月初，从深圳辞掉工作回到郑州。

我记得四年前的文彤彤说：我超级喜欢吃栗子。

  <br/>

我拉着胖仔出门。

两个人骑着电动车走遍了经八路文化路又走到金水路，还是没找到哪里可以买糖炒栗子。

  <br/>

终于在大石桥东南角找到一家坚果店，招牌上说有糖炒栗子可以卖！

我要买糖炒栗子。

店老板说：没有！

我抬头看看招牌上的霓虹闪烁。我指着宣传幅问老板：凭什么！

老板告诉我：在这夏末秋初的当口，陈年栗子卖完，新年栗子还未成熟。每年这时候都没有。

老板还告诉我：你就是跑遍郑州市也没有！

  <br/> 

胖仔拉着我要离开。

我不服。照旧立在店门口，盯着老板看他打理坚果。

店老板是个身材高大的肥胖子。

他抬起头来。那张脸超级像冯绍峰。

我告诉老板：冯绍峰是我超级喜欢的演员。

可惜你的身材毁了冯绍峰在我心中的形象。
 <br/>
 

店老板嗤之以鼻。不屑搭理我。

我追问他，兄弟怎么称 呼。

免贵姓杨，单名一个洋字。
 <br/>
 

我猛地抬头。

你不仅毁了冯绍峰帅气的形象。

你还毁了杨洋在我心中小鲜肉的形象。艹。
 <br/>
 

店老板果然是个骗子。胖仔我俩终于在几条街外买到糖炒栗子。

去往经八路的时候，我催促着胖子骑快点。再快点。

经八路向南去，越过黄河路，又越过纬一路。终于越过金水路。

一年没有见过文彤彤，糖炒栗子还有余热。

我没想到的是，去深圳一年，文彤彤的感情再也没有余热可以温暖我。

 
 <br/>
文彤彤把一袋子糖炒栗子递给我：以后不要再来找我了。分开一年，我想我们已经回不去了。

我一脸懵逼：我这不是回来了么，不过一张火车票的小事！

文彤彤没有再回答我。把糖炒栗子递给胖仔。

文彤彤告诉他：胖仔，帮我照顾他。

  <br/>

胖仔接过糖炒栗子吧唧吧唧吃的香甜。文彤彤转身走向办公楼。

我立在那里看她离去的背影，像孤零零地立在荒无人烟的原野。四面楚歌，风从不知名的方向吹过来，不冷不燥，但是催人泪。

  <br/>

我看她的背影，消失在转角。又回头看胖仔。

胖仔吧唧着嘴巴告诉我：真好吃，你要不要吃？

我一脚踹断他电动车的挡泥板，夺过来半包糖炒栗子。

我剥开一个放进嘴里，香甜中伴着咸味。我说：真特么好吃。草。

  <br/>

胖仔说：大老爷们，哭你麻痹，上车，带你去一个厉害的地方。

胖仔带我去的地方果然好厉害，是郑州市的大海上城，能脑补出傍晚时分人潮拥挤的模样。

  <br/>

其实大上海城就在文彤彤公司后面不远的地方。胖仔点满一桌子菜，又叫老板烤了大腰子。

菜还没上，酒已下去半箱。胖仔安慰我说：劳资没钱，你请客。

我问胖仔，你这是在安慰我么？

胖仔咕咚咕咚地喝酒，醉眼迷离地告诉我：我不管，反正你请客。

  <br/>

我抬起头看向文彤彤公司的方向，虽然离的近，但还是被建筑物遮挡，完全看不到。

  <br/>

胖仔安慰我：时间是药，你终将被治愈。

电话在桌子上嗡嗡地震动，铃声是那首漂洋过海来看你。

  <br/>

我随着铃声唱起自己改编的词句：

为你我用了一年的积蓄，翻山越岭地来郑州。

为了这次见面，我特意买了你最爱吃的，糖炒栗子。
 <br/>
 

胖仔撸一口大腰子骂我：闭嘴，你唱歌，真尼玛难听。

  <br/>

我接起妈妈打来的电话。

她在电话那头告诉我，在驻马店看到有卖电动车的，想买一辆给文彤彤上班用。

我说：不用买，她有电动车。

妈妈又嘱咐我要记得去探望文彤彤的爸爸，要记得买礼品。

我迷迷糊糊地说，好，好，好。

妈妈又问些乱七八糟的事情，无非是旁敲侧击地催着结婚。

我答应她，和文彤彤已经商量好，年底工作不忙，回家结婚。

  <br/>

胖仔载我回去的路上等红绿灯。我喝醉了唱乱七八糟的歌。

我唱：

是不是对生活，不太满意

很久没有笑过，又不知为何。

  <br/>

胖仔满嘴酒气地骂我：闭嘴，草！

  <br/>

我不理他，继续唱：

既然不快乐，又不喜欢这里

不如一路向西 ，去大理

  <br/>

胖仔骂我：操你大爷，难听死了。闭嘴。

  <br/>

温热的风吹的我醉意浓浓，我不理他，继续唱：

路程有点波折，空气有点稀薄

景色越辽阔，心里越寂寞。

  <br/>

这次胖仔没有再骂我，他突然加足马力，不等红灯绿灯，冲着疾驰而来的公交车径直撞过去。视死如归，同归于尽！

公交车在十字路口一个急刹车，柏油路上摩擦出长长的轮胎痕迹。整个车厢里的乘客惊叫起来。我听到身后的公交司机打开窗户冲着我俩骂脏话！

  <br/>

我紧紧抓住胖仔的大肚腩，破口大骂！骂他喝醉了不要命。

胖仔说：说了闭嘴，还唱，真特么难听，草。

  <br/>

九月末的某个傍晚，街道上飘满了桂花香。文彤彤打电话让我过去。

文彤彤把我的行李从她的房间收拾利索，等着我去搬行李。

我骑着胖仔改装的极其酷炫的电动车去找她。

文彤彤把行李递给我说：你走吧，祝你幸福。

我接过行李说：我真特么的幸福。

眼泪不争气地吧嗒掉下来。
 <br/> <br/>
 

我骑着电动车往回走，天色黑下来，经五路红专路的十字路口有很多烧纸钱的人。

大约是天气渐冷，世上的人户外活动的少了，另一个世界的人就开始出来闲逛。

所以子孙们要烧纸钱以示孝敬，同时也表达着祈求保佑。

  <br/> <br/>

我骑着电动车载着行李冲过去，从一堆堆燃烧着的纸钱堆上压过去。像个神经病！
 <br/> <br/>
 

我听到身后烧纸钱的人破口大骂，极其难听的脏话夹杂着性器官，问候我的祖宗。

  <br/> <br/>

我把车停下来，打开电动车周身新改装的爆闪灯。冲他们骂回去：劳资闪瞎你们狗眼。

  <br/> <br/>

在那一群烧纸钱的人冲过来要打死我之前，我又骑上电动车跑掉了，一路亮着爆闪灯向东去。

我回头看一眼地上被压灭的纸钱冒着浓浓的黄色烟雾，打电话给胖仔和木易。

我说：以后别骂我总干不冒烟的事情。我今天干的事情冒了很浓很大的烟！

  <br/> <br/> <br/>

大小姐曾告诉我：不多，你这样的人，百无禁忌的，抽烟喝酒熬夜，放心吧，会死很早的。

  <br/> <br/> <br/>

 

后来。我又去了多年前去过的那家小饭店。

我夹烧茄子放进嘴里，大声冲老板吆喝：老板，真好吃。

老板说：慢点吃，你嘴角都是米粒。

  <br/> <br/> <br/>

我突然落下泪来。冲动着拨出曾经熟悉的电话，忐忑地等，最怕标准的普通话告诉我：您好，你所拨打的是空号。
 <br/> <br/> <br/>
 

真庆幸，没有提示我说是空号。

真不幸，铃声自动结束了还没人接起来。
 <br/> <br/> <br/>
 

我发消息说：我想结婚了。我特么的要跟你结婚！

  <br/> <br/> <br/>

我想，如果号码早就弃用，那会不会被陌生人回复我，然后骂我神经病。
 <br/> <br/> <br/>
 

真庆幸，并没有人骂我神经病。

真不幸，并没有人回复我消息。
 <br/> <br/> <br/>
 

我打给木易说：快来快来，开着胖仔的保时捷来，带我去一个牛逼的地方。

木易骑着电动车来的时候问我：去哪里去哪里。

木易还告诉我，这辆车已经换了新的名字，它已经不叫保时捷了，新名字叫法拉利恩佐。

 
 <br/> <br/> <br/> <br/>
木易骑着胖仔的法拉利恩佐载着我沿着红旗路一路向东去。

我在后座上催促着：快点，再快点。

木易骂我：这特么已经50码了！

到花园路的时候我又催他：冲啊，怕什么！

木易骂我：红灯啊，草泥马！

  <br/> <br/> <br/> <br/>

我拎着两箱莫斯利安和一袋苹果去敲门。

木易一脸懵逼地问：这特么是哪？

我一边进门一边低声告诉木易：这是我老丈人！

木易说：操你大爷！

  <br/> <br/> <br/> <br/>

跟叔叔聊这几年的近况，聊家常小事，聊郑州快要完工的农业路高架！

直到聊到很晚文彤彤才回家。

文彤彤推门进来明显地吓一大跳。

文彤彤把我俩请出门外的时候说：你就这样冒昧地直接来我家？

那是分手后第一次再见到她。也是最后一次！

  <br/> <br/> <br/> <br/>

我解释说：我打电话给你，没人接。

我又说：我发信息给你，你没回。

我本想再说一句：我发的内容是，我想结婚了，跟你。

 
 <br/> <br/> <br/> <br/>
可是我还没来得及说出口，文彤彤就告诉我说号码早就换了，然后转身走掉。

  <br/> <br/> <br/> <br/>

木易递上来一支烟说：你说的带我去一个牛逼的地方就是来见老丈人？

木易说：劳资要喝莫斯利安，劳资要吃苹果。

我说：滚。

 
 <br/>
 

每次喝酒的时候，兄弟们都会拿玉今的故事嘲讽木易。

每当我嘲讽木易的时候。木易都会告诉我：喝完这杯酒，我带你红旗路，那里又一个牛逼的地方！

 
 <br/>
 

春节将至，妈妈打电话来：我找算命的算过了，年底结婚是个好日子。什么时候回来。

我沉默了一会说：感谢算命的。年底结婚！

  <br/>

可是鬼知道年底来的这么快，我记得有个傻逼说，时间如白驹过隙，倏忽而已，草。

  <br/>

妈妈频繁地打电话，我频繁地答应她：挺好的，挺好的。

  <br/>

前两天回到胖仔租在红专路42号院的房子里，棉衣脏了才发现没有衣服可以换洗。

胖仔翻箱倒柜地找出我的行李来。

他一件件拿出我的衣服，很仇富地骂我：妈的，买衣服只挑贵的，你真有钱。

他又翻出一个鞋盒子来，里面是一双崭新的安踏球鞋。

胖仔说，你特么也配穿几百块的鞋子？

  <br/>

我怔怔地看胖子翻我的大包行李，才知道那里面有好多新买的衣服，从秋装到冬装。唯恐我照顾不好自己的生活，唯恐我没有温暖的衣服可以度过漫长且寒冷的冬天。

我试穿每一件新衣服，不胖不瘦，不高不低，尺寸刚刚好。

我真特么幸福，草。

我又拿过来球鞋试穿，里面竟然摸出一千块钱。

我真有钱，草。

  <br/>

再有文彤彤的消息是听说她相亲的对象还算不错，是个优秀的大男孩。

我跟胖仔讲这件事情的时候并没有妒忌或者恨意。

因为我知道

当文彤彤需要爱情的时候，我说：我饭都吃不饱，哪有钱买衣服。

当文彤彤需要婚姻的时候，我拿着尚有余热的糖炒栗子给她爱情。

当我想给她婚姻的时候

文彤彤告诉我 ，我们不在一条时间线上，你的婚姻，我不想要了。

文彤彤还告诉我：对不起，我用尽自己的青春把所有的好给了你。现在我要离开了。

  <br/>

据说，养成一个习惯21天就够了。失去一个人，迷失的时间也是21天。

胖仔说：时间是药，终将治愈你。

狗屁，是药在时间里。

  <br/>

此后的岁月长长，故事慢慢写。今天，暂且搁笔。

 