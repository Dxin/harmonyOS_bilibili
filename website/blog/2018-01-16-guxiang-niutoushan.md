---

slug: 故乡 · 牛头山

title: 故乡 · 牛头山

authors: [mayuanwei]

tags: [人生杂谈]

---

豫东南有一去处，位于南阳、舞钢、驻马店的三市交汇处。

此地属大别山系的桐柏山余脉，也有讹传说是太行山脉。山头众多，却不甚伟岸。形状各有异同，远观似兽似物，神韵倒也不至难看。

---


<!--truncate-->
稍微高大些的一座山头，当地人叫它：牛头山。因其绵延的山体尽头凸出一高耸悬崖而得名。那悬崖酷似牛头，远看若有牛角，清晨雾气重，牛头山若隐若现地反刍着浓郁树木，其形象可真真是怪诞。牛头山的悬崖有十几丈高，这个数据是四爷爷告诉我的。那时候我还是个稚子孩童，尚不知长度单位该用什么表达。爷爷说十几丈高，我就如此记忆。后来读了书，才知道长度的计量单位是米。今时今日想来，大概是时代不同的缘故罢了。

 
   <br/>



我小的时候，那座山发生过一件灵异的事情。那个夏日的早晨，牛头山直愣愣地一道亮光从牛头处升起，直耸云霄。村里人都亲眼所见那景象，说是天子刘秀显灵了。

当时有一外乡人，籍贯广西。在牛头山造窑伐木烧炭。据说懂些术数，那日他徒手攀上牛头山悬崖，从上面摘得一枚石灵芝。

那枚石灵芝我们都见过，长约一米。明明是灵芝，却有着人参的样子，双腿双手清晰可见，乳房凸起，腰间部位稍微细了点。顶部是灵芝叶子，叶子上隐约有五官，不甚清楚，似面容较好的少女。

那外乡人得了灵芝，当天离开了村里。后来又回去过一次。给我们留下几张灵芝的照片以示馈赠。

据说他找专家化验过，说是灵芝长了八百年。有延年益寿起死回生的功效。有人花数百万购买，不卖。

他还说，他找到那枚灵芝的时候，整棵灵芝长在石头缝里，无水无土。阳光下有人影显现。如果长到一千年，灵芝就会吸收天地精华幻化成人。

    <br/>

延年益寿起死回生的药效自然是不可信的，吸收天地精华幻化成人之类的说法自然也是信口雌黄。我这人向来笃信科学从不迷信。若不是当真见过那灵芝的神奇模样，以及那些照片在叔叔家里时常翻看过。这样的事情，讲给我，我自然是不屑于相信的。

    <br/>

关于灵芝，小时候听祖辈说过，分为木灵芝，石灵芝，肉灵芝。木灵芝我见得多了，少年时候满山玩耍，采摘过不少，不过是上等草药，算不得什么稀罕玩意。祖辈说石灵芝可比木灵芝贵重多了，百年难得一遇。若是肉灵芝，更是可遇不可求的灵物了。肉灵芝还有个别名，叫太岁。

   <br/>

我对祖辈的这些迷信说法，向来觉得都是不存在的笑谈罢了。倒不如好好讲讲记忆中的牛头山。

    <br/>

 


在南阳及驻马店地区，有着许多《王莽撵刘秀》的系列故事广为流传。我自幼便听着这些传说长大，现在看来，不过是传说。但是年幼时听祖辈讲来，可是当真了来听的。其中情节，仿佛就在身边真切发生过，煞有其事，深信不疑。

    <br/>

关于《王莽撵刘秀》的传说，网络上亦收录了不少，叩问度娘，可见一斑。

   <br/>

传说是这样讲的：众所周知的，汉朝是刘姓天下。西汉东汉的历史节点，奸臣王莽篡夺了汉朝的皇位，建立新朝。刘秀起兵夺回天下，自此开创了两百年的东汉盛世，彼时连年战乱，刘秀的聚集起来的民兵大多时候被王莽的官兵追杀，刘秀逃命的几年时间，便流传下了无数脍炙人口的故事。这便是《王莽撵刘秀》的由来了。
   <br/>
 

当日刘秀身骑一头即将生产的雌骡，手里悠哉悠哉地剥开刚从农家田地里拔来的两株豌豆，正是豌豆成熟的季节，生长地圆润饱满，真命天子刘秀吃地甘甜可口。突见身后追兵将至，一时慌张，随手把豌豆抛在空中，挂在了路旁的槐柳树上。双腿加紧骡子的腹部，手里抓住骡子脖颈上的长毛，心惊胆战地祈祷胯下骡子能跑地快点。

    <br/>

偏偏就在关键时刻，骡子产期将至，尾巴高高翘起，要下崽了。

刘秀骑在骡背上着急地抓耳挠腮，愤愤地感慨：早不生晚不生，偏偏这个时候生。你这头蠢骡子，如果不会下崽该多好。

    <br/>

那骡子听到真命天子的咒骂，一时变异，小崽子也不生了。驮着刘秀飞奔而去。

    <br/>

真命天子，金口玉言，说出口的旨意自当灵验！所以，此后天下间的骡子都不会生产了。

我小时候，村里养的有骡子，还真是不会下崽。为此，我对这样的传说深信不疑好多年。后来高中读了生物，才明白了那是基因和染色体的道理。
   <br/>
 

还记得那两株被天子刘秀顺手一挥挂到槐柳树上的豌豆么？

在我的故乡，到处可见一种树木，至今我不曾知晓它学名，只是长的粗壮，夏日炎炎，枝叶茂盛时候，会长满树的类似于豌豆的果子，这也是刘秀的缘故了。
   <br/>
 

传说到此可没完，我讲给你后半段。

刘秀骑上那头骡子被追兵赶到一座山头，刘秀弃骡上山，被追兵逼到走投无路的时候，刘秀跺着脚说：山神啊山神，你要是能平地起高崖该多好哇！

    <br/>

山神听来天子的感慨，不敢有违圣意。即刻在山头直挺挺地长出十余丈的悬崖来。那悬崖笔直而上，棱角极少，人力无法攀爬。追兵无功而返。后来刘秀乘龙而去，以致后来夺回汉朝天下。再后来，便有了东汉王朝，以及《大汉天子》的电视剧了。
   <br/>
 

你大概已经想得出，这便是开篇所述牛头山了。

 
   <br/>

   <br/>   <br/>

牛头山倒也没什么极致的风景，不过是陪伴我童年生长的故乡，便是一种别样的喜欢了。

    <br/>   <br/>

记忆里还有一方深潭，按照方位来算，那深潭应是在牛肚子下面了。是一处不太大的池塘，不过深了些。小时候的夏天，每天都要脱光了在水里戏耍半日。倒不是为了乘凉，更多的是娱乐。后来读了大学，跟朋友第一次去游泳馆，他们看我会些游泳的技巧，都是从那池塘里学来的。

 
   <br/>   <br/>
这池塘有个吓人的名字，叫黑水潭。据长辈说是淹死过人。不知是真的淹死过，还是担心我们溺水，编出来吓唬我们的。不过八岁时候我确实差点淹死在里面，幸亏得村里大孩子及时潜入水底拉我出来。彼时年纪小，无知无畏地，后来年年夏日，照旧去戏耍，不曾中断，直到后来离开故乡，多年未归。

    <br/>   <br/>

牛头山的牛头处有悬崖，腹部有黑水潭。要说最好的去处，当属牛头山的尾巴。

 
   <br/>   <br/>
牛头山是真的有尾巴的，而且是银白色的，细长飘逸，远观如丝如带，算得一处美景。

那是方圆数十里独有的景致。

那尾巴是一条瀑布，高十余丈。

夏日缺水，旱灾时节，村人就用竹管到此处引水灌溉，顺着倾泻而下的重力，水能引到极远处的田间。这算大自然的馈赠了。

即便河水干涸时，瀑布不曾断流。祖辈说是山体里有暗河的缘故。
   <br/>   <br/>
 

 

秋天时候总要来几次山洪，山洪来时，瀑布暴涨，宽数丈。倾泻而下，其景其势，叹为观止。

这条瀑布有个直白的名字：响水台。

顾名思义，极远处都能听来那瀑布跌落的声音，哗啦啦，响在山谷里。小时候，我固执地认为那是大自然的歌唱，同龄人都对我嗤之以鼻。我认为是他们凡胎肉眼，不像我听得懂来自仙界的天籁。
   <br/>
 

响水台流经处，依着山腰，生长了一片毛竹，半亩有余。数量不算多，长得还算青翠浓郁，四月笋期，可食用，归胃经和肺经。味甘、清热化痰、益气和胃。

我有一个鬼点子，当竹笋刚破土的时候，拿罐子盖上，竹笋就会在罐子里弯弯曲曲地长满一大坨，不至于过早地生长成竹子。长辈不许，说是有违天道。
   <br/>
 

春天的迎春花，夏日的柳麦芽，秋天的映山红，冬日雪地里埋陷阱抓兔子。还有句俗话说：南坡到北坡，除却白草都是药。也算是对牛头山的多草本的描述了。

    <br/>

我对于牛头山的记忆还有许多，倘若真要一件件讲起来，那足够我们坐在山脚下的河床边生一堆篝火讲上几个时辰了。

故事讲完的时候，从山上田地里挖来的红薯，也就在篝火燃尽的炭灰中烤出漫天清香了。
   <br/>
 

2017/12/15童趣随笔。