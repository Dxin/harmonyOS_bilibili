---

slug: write to student

title: 写给大学生-曾许人间第一流

authors: [mayuanwei]

tags: [人生杂谈]

---
<b><center>写在最前</center></b>


我总是固执地将工作和公众号里的故事区分开来。仿佛这样就过了两种不同的人生：工作用来挣取填饱肚子的柴米油盐酱醋茶，故事就是我人生后花园的琴棋书画诗酒花。
<!--truncate-->


但是近几年来总在大学里工作，公众号粉丝也大多来源于此。我想有些话是时候通过不多讲故事来跟你们聊一聊。

这篇文章主要汇总于同学们平时来咨询我的问题。我其实比各位大不了十几岁，对于这个世界的理解也未必正确与深刻。我只是从我的角度来跟你分析，我故妄言之，你姑且听之。


<b><center>陷阱</center></b>

我想聊的第一个话题关于秩序、规则、公平、光明与阴暗。

有不少同学会来问我，自己专升本将将上岸或者仅仅差了三四分惜败。或者考研成绩过线又有些差强人意。此时有朋友可以通过关系暗箱操作。费用或高或低。

其实此类陷阱不止存在于专升本或者考研，如果认真调研起来，你会发现连高考甚至小升初都有雷同案例。每年的考试前后公安局派出所接到的报案也以此类居多。大家感慨骗子的手段越来越高明的同时，也鄙夷着受过高等教育的国之重器们怎么也会轻易上当受骗？

我没有加入过这种骗子组织，也没有受过他们的专业训练。我只从我了解的片面信息跟你们分享一下大概是怎么一回事。


张三同学专升本成绩刚好过线又没学可读，此时有人称可以通过李校长或者王院长的关系给你处理。费用多则数万，少则大千。张三同学身处毕业季的人生隘口，慌不择路地搭上线。也不是没有过怀疑，关键是对方所提供的所有信息全部真实，比如他们能清清楚楚地描述出你所有的个人信息，甚至你从侧面印证的时候，你的目标学校的负责人也真的就是李校长或者王院长。一套互相印证的信息链终于打消你的疑惑。当然了，之所以称之为暗箱操作，那自然是不能拿着真金白银跑到目标院校的校长办公室送钱的，由牵线搭桥的中间人收钱，即便被相关部门查处，也无法印证李校长与王院长有违纪行为。


所以你把钱花了。联系不到对方了。你发现被骗了。去派出所报案了。在派出所遇到了另外两个学校的张三同学在一起填表，你们的经历竟然雷同，此时你捶胸顿足，你嚎啕大哭，你说接受了小二十年的教育都没有社会的第一堂课来的真实。警察安慰着把你们送走。警察说他们会负责追查到底。警察说他们一旦破案第一时间通知你。


你在踏入社会的三五年后回忆起来这段经历，已经平复了心情，后来再次提及这段往事，你只会一边跟人碰杯一边拿年少无知自我开脱。却从未想过，是因为自己罔顾规则才落入坏人的陷阱。



很多时候人们总纳闷受过高等教育怎么还会被骗。其实你关注一下每年的315揭露的电信诈骗案例就会发现，连电线杆上的重金求子每年都有很多人上当。也就释然了。


我已经是三十来岁的人了，早就没了当年的少年感。我甚至可以理解并接受很多行业的忽悠欺诈行为了。但是我依旧偏执地认为:教堂的白鸽不会亲吻乌鸦，骗大学生的钱都没有亲妈。

 

我在公众号里写文章是你们网络远端的不多先生、我在互联网上录教程是你们的帝心同学、我在大学的课堂上讲课是你们的马老师。


我没有资格对你们的人生指手画脚。但是我有一点点的价值观可以分享给你。也是我一直坚持的东西，我按照这套未必正确但肯定无害的价值观去生活，它帮助我成为内心自由的人，我希望你可以参考一二。


如果你想要获得什么，就拿什么去换。这个世界本就如此公平。

如果有人杠我说，对呀对呀，我想要学历，所以我拿钱去买。这个逻辑没毛病啊。只不过遇上骗子了而已嘛。


我不太喜欢杠，但是我很会杠。如果你想要的是统招本科学科，那么你应该做的是在高考之前或者专升本之前付出努力来换取。而不应该想着只要我有钱就可以。


统招的学历含金量最高，如果连这也可以通过花钱来买。那这个国家交给各位国之重器我还真有点不放心。


我还总是误以为大学生代表着国之重器，后来才知道，更多人都是九年义务教育的漏网之鱼。我们把他们简称为：九漏鱼。


如果你继续杠我说，只要有人的地方就有关系，只要有钱就可以摆平关系，连国家教育部都可以给你安排的明明白白。那我只能说，我今年才三十来岁，眼界和思想都有很大的局限，那种高端的层次我还没能力涉足其中。


你知道吗？一个人的内在逻辑将会决定他一生的高度。

你知道吗？其实有很多东西都是钱买不到的。你没办法花钱买来知识，就像王思聪买不来主播的芳心。


我在给大学生上课的时候因为涉及到IT编程技术，其中总有很多绕人的逻辑，所以我总结了一套理论，被我教过的学生都知道这个理论有一个高大上的名字：马哥假设反推论证法。


假设钱可以买来统招的学历，假设花钱可以通过虚拟科技注入你脑子里知识。你的钱够吗？

你不要跟我讲：我读得起西亚斯和华信！！！

我一早就知道你们学费贵的离谱。


扯远了，做个总结吧。如果你没有考上目标院校。不要气馁。人生从来都不只有一条路可以走。不要因为当下恰好处在人生节点，身在其中容易一叶障目。心态不必太极端，凡事无绝对。心态好点，总会柳暗花明的。


你当年高考没考上本科，你以为人生无望。读专科的三年，过的不也蛮潇洒的嘛？

你现在没上岸成功，更不要觉得人生自此就终结。离开校园从来不是终点，只是你人生新征程的起点而已，我相信你所求皆如愿，所行皆坦途。

你的未来会更潇洒的。我嘴开过光，你可以信我。


如果你有被陷阱欺骗的经历，或者你身边有这样的案例，我希望你可以留言在评论区里警醒后来者，我替他们谢谢你。

 
<b><center>升本</center></b>


考上心仪的院校的同学大抵是不会被骗了。所以他们也不再问我花3万块钱找李校长调档案是否值得了。

他们更多的问题是问我升本应该去哪个大学！



去商丘学院好还是去黄科大好？

去郑州财经好还是去华信好？

去排名117的那个垃圾学校好还是去排名121的那个更垃圾的学校好？



我先声明，我只是为了凑排比格式所以才随手拿了四个学校凑行数。如果你非要举报我说是我在诋毁以上学校，我可不认！！！

如果我收到律师函，你就失去了不多先生或者马老师。请相信，失去我是你的损失。


算了，我他妈的再声明一次！！！

商丘学院和黄科大是非常好的本科学校。

郑州财经和华信学院是非常棒的本科学校。

我在这里都带过课。他们的学校非常非常好。真的！

 

所以升本应该去哪个学校呢？

我可以很负责地告诉你：你做一个转盘，把目标院校列出来。你随机选一个就是你应该去的大学。

我的学生原话是这样说的：淦。你这也算负责？

 

是的，因为不论你选哪一个。你都是不当人两年，收两年快递打两年游戏。毕业后拿到了你的本科证书。你拿着本科学历去跟HR面试的时候，对方并不关心你证书上学校的名字。他们只需要知道你是本科学历就够了。

因此，你是否理解了，去哪个学校已经不重要了？如果非要慎重选择，不如选个省会郑州的，快递有可能早一天收到，点外卖的时候也可能更好吃一些。



那现在还有最后一个问题给你解释。我在上文提到了一句话：不当人两年。

其实不止两年。你毕业以后进了电子厂或者销售岗位，你在永远都开不完的毫无意义的会议上突然想到，这他妈的的不是我读三年专科又升本两年的目标啊。这个傻逼领导就这么点事逼逼两个小时了。他压根都不拿我当人。

 

你知道吗？在我看来，升本的意义从来都不在于那一纸文凭。



升本的意义在于你还有两年的时间延缓踏入社会，你可以用这两年的时间改变自己从前拉跨的人生。你可以用这两年的时间让自己毕业就成为社会中的佼佼者。



有学生问过我。老师，我懂你意思了。本科随便选一个去读。

那么我该朝哪个方向努力呢。

目前互联网公司流行的是什么技术呢。

我应该学大数据还是学前端技术呢。



在这里，我想对很多迷惘的同学做一个规划。

这个规划分三个步骤。

我跟学生说过。你只要按照这三个规划过两年，哪怕在二线城市，我也相信你能一毕业就月薪8千不是问题。

也有人问过我，如果拿不到，可不可以让我补差价。

我第一次被问到的时候是有些懵逼的。

所以，我在这里跟你立下字据，如果你按照我的规划度过你的本科两年生活。不到8K的月薪，我补差价。



凭良心讲，超过8的部分我肯定是不会让你补给我的，不然也太不要脸了。但是在夏天请我吃个雪糕总不过分吧。我要5块钱起步的那种。

铺垫到这里，你肯定开始疑惑，怎样的两年规划可以毕业就高薪呢？



核心思想就一句话：做个人！

 

不用疑惑，我先跟你说什么叫不做人。

大学生手册我没有详细阅读过。但是在我的价值观里，应该是有不旷课，上课不睡觉，不玩手机等规定的。

所谓做个人：敬畏规则，有所为有所不为。

 

我现在要展开地跟你讲三个步骤是什么了

第一步：

你会遇到很多傻逼的队友，他们熬夜打游戏，他们旷课泡吧，他们上课玩荣耀下课打联盟，他们对规则没有最基本的敬畏，他们对老师没有最基本的尊重。

你不要成为这样的人。

哦不，你不要成为这样的傻逼。

你要认真听老师讲课。不论老师是很卖力地讲满整节课，还是讲了十分钟后面开始混时间。这不重要。你只要知道，老师在讲课的时候，你每一分钟都在认真听讲即可。


说到这里，就是三步规划中的第一步已经完成了。

你可能会说，妈的就这？

 

第二步：

你放心，你哪怕按照我给的建议开始做人，开始改变从前的混子人生，开始认真完成第一步好好学习了。

你会发现，并没什么卵用。

因为之所以有专升本这样的事情存在。那么本科的课程与专科一定有差别，马哥假设反推论证法说：假设本科所学课程和专科毫无二致，那么专升本存在的意义难道仅仅是为了收你学费给你一个本科学历吗，显然这个逻辑是不对的。



你开始在本科的课堂上学习IT相关的技术了。你终于做个人了。你发现你听不懂了。

就像小学数学老师教你100以内加减法你不学习，中学数学老师教你解方程你不学，有一天你的高数老师教你常微分方程了，你很努力的听课了，发现你并不听不懂。



所以你开始说，我真的有努力听老师讲Java框架的课，但是我在专科学Java基础的时候是混子，现在压根听不懂。不是我不想努力，是我真的跟不上了。



所以这就是你在升本以后的Java框架课堂上打王者的理由。你甚至给自己找好了说辞：本来是想学的，可是又听不懂，所以心里烦躁，这把0-9不是我真实的水平。

这！就是我在大学课堂所遇到的现场情况。

 

既然写了这篇文章，就应该告诉你解决办法。医学上说诊而不治是流氓就是这个道理。

你先跟着听课。我知道你听不懂的。没有关系。继续听，哪怕在后来的印象里能够想起老师逼逼过就够了。

这样坚持上完本科的课程就可以月薪8+吗？

你在想屁吃。



大学是平等、自由、知识的名片。但是大学获取知识最重要的途径绝对不止课堂这一种方式。


具体去哪些学习网站我在此处不必推荐。因为我相信你们收藏的学习网站比我的都多。

你学Java框架的时候因为从前没有学好Java基础，那就在课下补回来。

你会发现你还需要补C语言、数据结构、算法等等。

你会发现这他妈的按照马老师推荐的方法过本科生活压根就没有时间在周末的时候去和女朋友们开房了。压根就没有时间看LPL的夏季赛了。

是的，确实如此。但是为了毕业就月薪8+。为了按照这种方式过本科生活两年拿不到8找我补差价。我觉得你可以试试。两年很快就过去的。我等你的雪糕。五块钱以下的不要。

 

第三步：

你刚准备好开始本科的两年生涯。以上两步已经够你受用两年。如果你做到了。两年后再来拿着这篇文章问我第三步的具体内容是什么。

 
我接触的同学除了专升本上岸的之外。还有一些是刚读大一的，如果有高考刚结束准备报计算机相关专业的同学。你们如果也想毕业在二线城市就能够月薪8+。我也有方案推荐给你们。

如果你问我具体方案是什么。

我会告诉你：当个人。

 
<b><center>培训</center></b>


关于培训的问题也是目前咨询我最多的。但是我已经为此感到无比的悲哀。

因为我曾在IT培训机构工作四年的经历，结识了诸多培训机构的朋友，上到企业老总下到基层工作人员，大多机构都有相熟的朋友。因此我对每个机构的教程，教法，管理，运营，黑料等等问题都门清。

因我现在的工作主要是在大学计算机专业授课，所以每年的毕业生总有很多关于培训的问题，我无法表达其中的期盼与悲哀，只简单聊几句，以供你们参考。

很多人喜欢问：这个机构学费多少，住的是不是单间，管吃吗？

这些同学在他们老了以后，我就让马星河去卖保健品给他们。一卖一个准。

我更希望你会问：这个机构教哪些学科，学科内容涉及到的技术点都有什么，授课的方式是怎样的。平时学习的过程中遇到的问题如何反馈和解决的方案及效率。授课老师的授课风格和思路是否是我适应的。


13天前，我很看好的一个小兄弟在咨询我以后并未听从我的建议而是选择去了某不知名小机构培训。

他选择那家小机构的原因是因为他们收的学费是市场价的一半。这价格可以说是低的离谱。

13天后，他跑来跟我吐槽肠子都悔青了。这也是我想聊一聊我感到悲哀的原因之一。


悲哀的地方还有很多。

比如我很喜欢一个斗鱼主播叫放逐大帝。他打LOL的时候会和他的二弟以相声的方式跟观众互动。我在华信那所大学上课的时候认识了一个叫燕坤的同学，他也是放逐大帝的铁粉。所以我们模仿着主播的样子结拜了大哥和二弟。


二弟一早就跟我说他毕业后要培训。我一早就给他了整套Python教程去自学，同时在平日里我也对他多有关照，我们一起制定了适合他的学习方案。二弟大学毕业之前，他已然对这门技术有了很全面的认知。同时他家里有亲戚在python岗位工作，算是很资深的前辈。

二弟后来去按照我推荐的最适合他的机构去学习了这门技术。课程尚未结束，二弟就被企业高薪聘走。



半年后他的同学们陆续着急培训了。他们只有一个逻辑。二弟学的这个，工资很高。我也去这家机构，学这个。

有些人连四类八种基本数据类型都整不明白，只因为眼红自己的表哥培训了Java薪资很高就铁了心去学Java。



道理很简单。不要尝试复刻别人的人生。因为其中有很多细节是你无法知晓的。应该找到最适合自己的路。适合，才会事半功倍。


我总是跟每一个咨询我的同学分析他的技术基础，他的性格，他对技术的接受速度和程度，来给他推荐适合的机构和学科。


有些机构会教200个技术点。有些机构会教20个技术点。并不是教的多就代表着适合你。广度太平洋，深度小池塘的方式适合那些吸收度很快并且还能举一反三甚至还有精力自己深挖的同学。而有些同学则适合针对找工作够用即可但是会深入细致的学习方式。

这大概就是因材施教吧。



我总是期待着我用心给你分析，然后推荐。

但是实际情况却是：我辗转于各机构四年，我不靠你的学费赚钱。良苦用心推荐给你，你最终只问一个对行情并不熟悉的人，而且你的问法还极其荒诞：你能不能保证我几个月后月薪过万。

有些人总是很聪明，不见兔子不撒鹰。如果有人跟他保证去做一件事一定有对应的收获。他才开始去播种。否则绝对不会去开始。
有些人总是很蠢笨，把头埋低，低头赶路，但行努力，不问结果。

我见过太多聪明的人颗粒无收，而看似蠢笨的他们却柳暗花明。我就知道，人生从来都是事在人为。

 
<b><center>学历和技术哪个重要</center></b>


这是最后一个想聊的问题。我也没有标准答案。

我只知道，不论学历还是技术，但凡有一个，也不会问这样的问题。

那如果一个都没有呢。

那就提升啊。

学历虽然没通过专升本上岸，不还有其他的提升学历的方式吗。这几年我没少帮同学提升学历吧。

技术现在虽然还没有。那就努力补啊。

家里有矿还来问我这个问题的，我觉得你是在消遣我。因为这两个对你来讲，都不重要。

<b><center>写在最后</center></b>
 

我喜欢的一句诗词送给你各位大学生：

劝君莫忘少年志，曾许人间第一流


原文链接：[写给大学生：曾许人间第一流](https://mp.weixin.qq.com/s/e3cbR8HCbeR6FcjkEszpxg)