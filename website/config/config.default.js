const config = {
  url: "https://hm.codefe.cn",
  trailingSlash: true,
  organizationName: "鸿蒙学苑",
  icp: "豫ICP备2022004823号-1",
};

export default config;
