import * as Auth from "./AGCAuth";
import * as ShowCase from "./AGCShowCase";

export default {
  Auth,
  ShowCase,
};
