---
sidebar_position: 1
---

# 课程由来

## 帝心老师+庄生老师

![微信公众号](./img/docs_intro_wegzh.png)
![2.0 课程](./img/2.png)

![](./img/3.png)

![](./img/4.png)

![](./img/5.png)

## 课程特色

- 新版本 HarmonyOS4.0 教程
- 零基础保姆级教程
- 全部资源直接共享
- 华为 HarmonyOS 应用开发认证+开发
- HarmonyOS 布道师
- ...

## 课程大纲

1. 入门
2. 开发
3. 编译调试
4. 应用测试
5. 上架
6. 项目
