---
sidebar_position: 107
---
# 07：第一季收官寄语

[本小节适合点击此处链接听帝心老师逼逼](https://www.bilibili.com/video/BV1pb4y1g75m?p=48)


锦瑟无端五十弦，一弦一柱思华年。

**庄生**晓梦迷蝴蝶，望**帝**春**心**托杜鹃。

# 第一季：全网最新最细HarmonyOS4.0教程（日更）

![image-20240207004551590](img/image-20240207004551590.png)

# 第二季：HarmonyOS4.0实战教程



# 第三季：HarmonyOS-Next教程



沧海月明珠有泪，蓝田日暖玉生烟。

此情可待成追忆，只是当时已惘然。

---



[离别是为了更好的相逢哦](https://mayw-teaching.feishu.cn/docx/IGWBdIiNOolMQux0JahculIFnuh?from=from_copylink) 

