---
sidebar_position: 102
---

# 02：环境搭建

> DevEco Studio3.1版本（23年11-21编写本教程时最新版）
>
> 但是该版本bug比较多，遂建议使用4.0版本。安装过程不变。



# 开发工具下载安装

> 要求电脑内存8G以上，建议16+。如需视频指导安装过程，可点击下方超链接。
>
> **[环境搭建_指导视频](https://www.bilibili.com/video/BV1pb4y1g75m?p=3)**

## 下载安装包

1. [点击本链接进入openHarmony的官网借用其安装包下载并解压安装](https://docs.openharmony.cn/pages/v4.0/zh-cn/release-notes/OpenHarmony-v4.0-release.md#%E9%85%8D%E5%A5%97%E5%85%B3%E7%B3%BB)

![alt text](screenshots/session-1-02-image.png)
> 看不懂这个截图的三个超链接该选哪个的同学，自行问一下宿舍的义父。

![alt text](screenshots/session-1-02-image-1.png)

1. 一路next进行安装

![next](img/next.PNG)

![alt text](screenshots/session-1-02/image.png)

![alt text](screenshots/session-1-02/image-1.png)

![alt text](screenshots/session-1-02/image-2.png)

![alt text](screenshots/session-1-02/image-3.png)

---
## 安装完成开始配置环境
![alt text](screenshots/session-1-02/image-4.png)

![alt text](screenshots/session-1-02/image-5.png)

### 安装node和ohpm环境

> 很多同学因为自己电脑上曾经安装过node，便选用本地node。大可不必，你最好选择此处教程中临时安装的node，因为不同版本编辑器对不同版本node有要求。
> 
> 同一台电脑本就可以安装多版本node共存，切勿纠结此处。


![alt text](screenshots/session-1-02/image-6.png)

### 安装sdk环境

> 注意选择正经目录

![alt text](screenshots/session-1-02/image-7.png)

### 同意两个条款
![alt text](screenshots/session-1-02/image-8.png)

### 安装相关环境
> 保证网络稳定，不要动辄中断。如果校园网特别离谱。请用自己流量。

![alt text](screenshots/session-1-02/image-9.png)

![alt text](screenshots/session-1-02/image-10.png)

![alt text](screenshots/session-1-02/image-11.png)


## 环境校验

> 程序员一般情况都比较喜欢黄色，代表警告。不用担心。正常使用。
> 程序员一般最怕红色。代表错误。需要处理。

![alt text](screenshots/session-1-02/image-12.png)



---

# 卸载软件

> 安装失败原因很多人是因为网络不稳定。不要使用校园网（不稳定）或者内网（需要代理）。

如果安装失败，按照网络解决办法又处理不好。

可以卸载重装。推荐好用的卸载软件：[geek-最好用的卸载工具.exe](https://mayw-teaching.feishu.cn/file/PzOSbHyZOoObtmxEAeKcxgZ3nhg?from=from_copylink) 

如果卸载重装还不好使，那很可能需要重装系统。

# 开发者注册与个人实名认证

[点击此处链接，参考文档指导，完成开发者注册与**个人**实名认证。](https://developer.huawei.com/consumer/cn/doc/start/registration-and-verification-0000001053628148?ha_linker=eyJ0cyI6MTY5NjkyMDE3ODQ3MiwiaWQiOiIwYjdmODYzNjY3OGE5ZWY2MWE4MjRlYjk2ZjMxNTg5YSJ9)

> 不论是后续开发使用远程模拟器或者真机，还是考试认证。亦或者参加鸿蒙训练营获取证书。
>
>均需要有该账号，并且实名认证。不如提前完成。 