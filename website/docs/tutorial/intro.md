---
sidebar_position: 1
---

# HarmonyOS：资料汇总

日拱一卒无有尽，功不唐捐终入海。


## 工具-IDE
1. [官方下载 DevEco Studio NEXT](https://developer.huawei.com/consumer/cn/deveco-studio#download)

## 学习文档
### HarmonyOS
1. [鸿蒙开发者官网](https://developer.huawei.com/consumer/cn/)

2. [openHarmony官网](https://www.openharmony.cn/mainPlay)

3. [openHarmony应用开发（北向）](https://docs.openharmony.cn/pages/v4.0/zh-cn/application-dev/application-dev-guide.md)

4. [openHarmony设备开发（南向）](https://docs.openharmony.cn/pages/v4.0/zh-cn/device-dev/device-dev-guide.md)

5. [openHarmony应用UX设计规范](https://docs.openharmony.cn/pages/v4.0/zh-cn/design/ux-design/app-ux-design.md)


6. [ AppGalleryConnect 端云一体化AGC后台地址](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/)

7. [ AppGalleryConnect 端云一体化AGC指南](https://developer.huawei.com/consumer/cn/doc/AppGallery-connect-Guides/agc-get-started-harmony-arkts-0000001683915520)


8. [OpenHarmony三方库中心仓](https://ohpm.openharmony.cn/#/cn/home)

---
## 视频教程

### 官方视频教程

1. [官方视频教程(NEXT)](https://developer.huawei.com/consumer/cn/doc/harmonyos-video-courses/video-tutorials-0000001443535745)

    > 帝心：质量极好


2. [民间-帝心](https://www.bilibili.com/video/BV1pb4y1g75m/) 
    > 帝心：质量一般，但是对小白很友好。初学鸿蒙者非常适合。
    > 网友戏称：鸿蒙之父。全网最细。

3. [黑马-虎翼](https://www.bilibili.com/video/BV1Sa4y1Z7B1/)
    
    > 帝心：专业机构，质量超级好。强烈建议！
    
4. [尚硅谷-刘波](https://www.bilibili.com/video/BV1Ti4y1s79B/)
    
    > 帝心：专业机构，质量超级好。强烈建议！


5. [黑马-蒋鹏](https://www.bilibili.com/video/BV14t421W7pA/)
    
    > 帝心：专业机构，质量超级好。强烈建议！
    
6. [python大本营](https://www.bilibili.com/video/BV1Aj421f7xK/)
    
    > 基础课（帝心）+电商严选（肖斌）



7. [黑马-满一航-端云一体化](https://www.bilibili.com/video/BV1Aj421f7xK/)
   

## 鸿蒙认证

### HarmonyOS应用开发：职业认证(收费)

1. [华为职业认证](https://e.huawei.com/cn/talent/cert/#/careerCert)

2. [HCIA-HarmonyOS Application Developer](https://e.huawei.com/cn/talent/#/cert/product-details?certifiedProductId=719&authenticationLevel=CTYPE_CARE_HCIA&technicalField=BSH&version=2.0)
    鸿蒙`应用`开发：中级

3. [HCIP-HarmonyOS Application Developer](https://e.huawei.com/cn/talent/#/cert/product-details?certifiedProductId=607&authenticationLevel=CTYPE_CARE_HCIP&technicalField=BSH&version=1.0)
    鸿蒙`应用`开发： 高级

4. [HCIA-HarmonyOS Device  Developer](https://e.huawei.com/cn/talent/#/cert/product-details?certifiedProductId=717&authenticationLevel=CTYPE_CARE_HCIA&technicalField=BSH&version=2.0)
    鸿蒙`设备`开发：中级

5. [HCIP-HarmonyOS Device Developer](https://e.huawei.com/cn/talent/#/cert/product-details?certifiedProductId=609&authenticationLevel=CTYPE_CARE_HCIP&technicalField=BSH&version=1.0)
    鸿蒙`设备`开发： 高级

### HarmonyOS应用开发：开发者认证（免费）
1. [开发者能力认证官网](https://developer.huawei.com/consumer/cn/training/dev-certification/a617e0d3bc144624864a04edb951f6c4)

2. [HarmonyOS应用开发者基础认证](https://developer.huawei.com/consumer/cn/training/dev-cert-detail/101666948302721398)

3. [HarmonyOS应用开发者高级认证](https://developer.huawei.com/consumer/cn/training/dev-cert-detail/101684223987951077)

4. [开发者认证题库](../tutorial/resources/session-4-00.md)
