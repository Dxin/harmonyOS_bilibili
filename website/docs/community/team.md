---
sidebar_position: 0
id: team
sidebar_label: 团队
slug: /community/team
---

# 团队

import {
ActiveTeamRow,
HonoraryAlumniTeamRow,
StudentFellowsTeamRow,
} from '@site/src/components/TeamProfileCards';
import Imgs from '@site/src/components/Imgs';

## 核心成员

核心成员致力于系列教程的设计、备课、录制视频等核心任务。

现有核心成员按字母顺序列出如下：

<ActiveTeamRow />

## 特别鸣谢

本鸿蒙系列视频教程最初由 帝心&庄生 打造。 时至今日，已经有数十位贡献者自愿主动参与。 我们希望对过去向本鸿蒙教程各个环节做出贡献的众人表示感谢：

- 周道 [哔哩哔哩](https://space.bilibili.com/260406695)
- 英雄哥 [](#)
- 小孙同学 [](#)
- 荒天帝 [](#)
- 崔东玉 [](#)

## 感谢支持

:::tip

为众人抱薪者不可使其冻毙于风雪。

课程全部免费，大家也完全可以拿去传播。但如果能因此获利，我将备受鼓舞。

量力而行！！！心意即可！！！

:::

<Imgs />
